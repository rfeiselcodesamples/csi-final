import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TrafficbytesComponent } from './components/trafficbytes/trafficbytes.component';
import { TimedatasentComponent } from './components/timedatasent/timedatasent.component';

const routes: Routes = [
  { path: '', redirectTo: '/traffic', pathMatch: 'full' },
  { path: 'traffic', component: TrafficbytesComponent },
  { path: 'timebytes', component: TimedatasentComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
