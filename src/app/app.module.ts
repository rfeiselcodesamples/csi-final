import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './/app-routing.module';

import { NvD3Module } from 'ng2-nvd3';
import { MomentModule } from 'angular2-moment';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { FakedataSource } from './data/fakedata.source';

import 'd3';
import 'nvd3';

import {
  MatButtonModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule
} from '@angular/material';

import { AppComponent } from './app.component';

import { TimedatasentService } from './services/timedatasent.service';
import { TrafficbytesService } from './services/trafficbytes.service';
import { TrafficbytesComponent } from './components/trafficbytes/trafficbytes.component';
import { TimedatasentComponent } from './components/timedatasent/timedatasent.component';

@NgModule({
  declarations: [
    AppComponent,
    TrafficbytesComponent,
    TimedatasentComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    NvD3Module,
    MomentModule,
    HttpClientInMemoryWebApiModule.forRoot(
      FakedataSource)
  ],
  providers: [
    TimedatasentService,
    TrafficbytesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
