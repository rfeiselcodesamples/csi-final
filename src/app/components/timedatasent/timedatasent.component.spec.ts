import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimedatasentComponent } from './timedatasent.component';

describe('TimedatasentComponent', () => {
  let component: TimedatasentComponent;
  let fixture: ComponentFixture<TimedatasentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimedatasentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimedatasentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
