import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TimedatasentService } from '../../services/timedatasent.service';
import * as moment from 'moment';

declare let d3: any;

@Component({
  selector: 'app-timedatasent',
  templateUrl: './timedatasent.component.html',
  styleUrls: ['./timedatasent.component.css',
  '../../../../node_modules/nvd3/build/nv.d3.css'],
  encapsulation: ViewEncapsulation.None
})
export class TimedatasentComponent implements OnInit {
  height = 450;
  results: any[];

  options = {};
  data: any[];

  constructor(private timedatasentService: TimedatasentService) { }

  ngOnInit() {
    this.getTimeDataSent();

    this.options = {
      chart: {
        type: 'lineChart',
        height: this.height,
        margin : {
          top: 20,
          right: 20,
          bottom: 50,
          left: 55
        },
        x: function(d){
          return d[0].toDate();
        },
        y: function(d){
          // present as 'KB'
          return d[1] / 1000;
        },
        showValues: false,
        duration: 500,
        xAxis: {
          axisLabel: 'Time',
          tickFormat: function(d) {
            return d3.time.format('%X')(new Date(d));
          },
          showMaxMin: false
        },
        yAxis: {
          tickFormat: d3.format(',f'),
          axisLabel: 'Data Sent (KB)',
          axisLabelDistance: -10
        }
      }
    };
  }
  buildVM(): void {
    // build line data view model
    const linedata: any[] = [];
    // use IP addresses as each line key
    Object.keys(this.results[0].values).forEach((key:string, i) => {
      const lineValues: any[] = this.results.map(d => [d._time, d[key]]);
      linedata.push(
        {
          'key': key,
          'values': lineValues
        }
      );
    });
    this.data = linedata;
  }
  getTimeDataSent(): void {
    this.timedatasentService.getTimeDataSent()
    .subscribe(timedatasent => {
      // pluck result
      this.results = timedatasent.map(timedata => timedata.result );

      this.results.map(timedata => {
        // convert date string to 'moment'
        timedata._time = moment(timedata._time);

        // extract line values ( and keys )
        const { _time, ...values } = timedata;
        timedata.values = values;
      });
      this.buildVM();
    });
  }

}
