import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrafficbytesComponent } from './trafficbytes.component';

describe('TrafficbytesComponent', () => {
  let component: TrafficbytesComponent;
  let fixture: ComponentFixture<TrafficbytesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrafficbytesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrafficbytesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
