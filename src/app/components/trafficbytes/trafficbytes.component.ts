import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TrafficbytesService } from '../../services/trafficbytes.service';

declare let d3: any;

@Component({
  selector: 'app-trafficbytes',
  templateUrl: './trafficbytes.component.html',
  styleUrls: ['./trafficbytes.component.css',
  '../../../../node_modules/nvd3/build/nv.d3.css'],
  encapsulation: ViewEncapsulation.None
})
export class TrafficbytesComponent implements OnInit {
  height = 450;
  options: any = {};
  data: any[] = [];

  top100: any[];

  constructor(private trafficbytesService: TrafficbytesService) { }

  ngOnInit() {
    this.getTrafficbytes();

    this.options = {
      chart: {
        type: 'discreteBarChart',
        height: this.height,
        margin : {
          top: 20,
          right: 20,
          bottom: 50,
          left: 55
        },
        x: function(d){
          return d['All_Traffic.dest'];
        },
        y: function(d){
          return d.sum_bytes / 1000;
        },
        showValues: false,
        xAxis: {
          axisLabel: 'Destination',
          tickValues: []
        },
        yAxis: {
          tickFormat: d3.format(',f'),
          axisLabel: 'Sent (KB)',
          axisLabelDistance: -10,
          ticks: 5
        },
        tooltip: {
          contentGenerator: (key) => {
            const value = key.data;
            return '<h3>Bytes Sent</h3>' +
              '<h2>' + d3.format(',f')(value.sum_bytes) + '</h2>' +
              '<div>Src: ' + value['All_Traffic.src'] + '</div>' +
              '<div>Dest: ' + value['All_Traffic.dest'] + '</div>';
          }
        }
      }
    };
  }
  buildVM(type: string): void {
    this.data = [
      {
        key: 'bytes',
        values: [
          ...this.top100
        ]
      }
    ];

    const y = d3.scale.linear()
    .domain(d3.extent(this.data[0].values, function(d) { return d.sum_bytes; }))
    .range([this.height, 0]);

    this.options.chart.yAxis.scale = y;
  }
  getTrafficbytes(): void {
    this.trafficbytesService.getTrafficbytes()
    .subscribe(trafficbytes => {
      // pluck 'result'
      const results = trafficbytes.map(t => t.result);
      // convert types
      results.map(t => t.sum_bytes = +t.sum_bytes);
      // store top100 collection
      this.top100 = results.sort((a, b) => b.sum_bytes - a.sum_bytes).slice(0, 100);
      // build view model
      this.buildVM('top100');
    });
  }
}
