export interface TrafficBytes {
  preview: string;
  result: {
    'All_Traffic.dest': string;
    'All_Traffic.src': string;
    'sum_bytes': number;
  };
}
