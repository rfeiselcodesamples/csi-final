import { TestBed, inject } from '@angular/core/testing';

import { TimedatasentService } from './timedatasent.service';

describe('TimedatasentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TimedatasentService]
    });
  });

  it('should be created', inject([TimedatasentService], (service: TimedatasentService) => {
    expect(service).toBeTruthy();
  }));
});
