import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { TimeDataSent } from '../models/timedatasent.model';

@Injectable()
export class TimedatasentService {
  private endpoint = '127.0.0.1/timedata';

  constructor( private http: HttpClient ) { }

  /** GET TimeDataSent from the server */
  getTimeDataSent (): Observable<TimeDataSent[]> {
    return this.http.get<TimeDataSent[]>(this.endpoint)
    .pipe(
      tap(result => console.log(`fetched TimeDataSent`)),
      catchError(this.handleError('getTimeDataSent', []))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
