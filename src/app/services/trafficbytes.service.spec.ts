import { TestBed, inject } from '@angular/core/testing';

import { TrafficbytesService } from './trafficbytes.service';

describe('TrafficbytesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TrafficbytesService]
    });
  });

  it('should be created', inject([TrafficbytesService], (service: TrafficbytesService) => {
    expect(service).toBeTruthy();
  }));
});
