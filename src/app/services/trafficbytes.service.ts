import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { TrafficBytes } from '../models/trafficbytes.model';

@Injectable()
export class TrafficbytesService {
  private endpoint = '127.0.0.1/trafficbytes';

  constructor( private http: HttpClient ) { }

  /** GET TrafficBytes from the server */
  getTrafficbytes (): Observable<TrafficBytes[]> {
    return this.http.get<TrafficBytes[]>(this.endpoint)
    .pipe(
      tap(result => console.log(`fetched TrafficBytes`)),
      catchError(this.handleError('getTrafficBytes', []))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
